package com.example.demo.model;

public class ErrorLocation {
    private String url;
    private String location;
    private String note;
    private Long errorId;

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public String getLocation() {
        return location;
    }

    public void setLocation(String location) {
        this.location = location;
    }

    public String getNote() {
        return note;
    }

    public void setNote(String note) {
        this.note = note;
    }

    public Long getError_id() {
        return errorId;
    }

    public void setError_id(Long error_id) {
        this.errorId = error_id;
    }
}
