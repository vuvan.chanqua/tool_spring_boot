package com.example.demo.controller;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

import javax.servlet.http.HttpSession;

@Controller
public class MainController {

    @RequestMapping("/")
    public String index(HttpSession httpSession){
        String root = System.getProperty("user.dir");
//        try {
//            Date date = new Date();
//            SimpleDateFormat formatter = new SimpleDateFormat("dd-MM-yyyy_hh-mm-ss");
//            String strDate = formatter.format(date);
//            String folderName = root + "/" + strDate;
//
//            File file = new File(folderName);
//
//            if (!file.exists() && !file.isDirectory()) {
//                file.mkdir();
//                String fileName = folderName + "/data.json";
//                Path fileJson = Paths.get(fileName);
//                Files.write(fileJson, "".getBytes());
//                httpSession.setAttribute("filePath", fileName);
//            }
//        } catch (Exception e) {
//
//        }
        return "index";
    }

    @RequestMapping("/folder")
    public String getRootPath(HttpSession session){
        return session.getAttribute("filePath").toString();
    }

}
