package com.example.demo.controller;


import com.example.demo.entity.ErrorBo;
import com.example.demo.entity.ErrorFileBo;
import com.example.demo.entity.ErrorLocationBo;
import com.example.demo.entity.PentestBo;
import com.example.demo.mapping.Mapping;
import com.example.demo.model.Error;
import com.example.demo.model.ErrorFile;
import com.example.demo.model.ErrorLocation;
import com.example.demo.model.Pentest;

import com.example.demo.service.error.ErrorService;
import com.example.demo.service.error_file.ErrorFileService;
import com.example.demo.service.error_location.ErrorLocationService;
import com.example.demo.service.pentest.PentestService;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;
import java.util.List;

@org.springframework.web.bind.annotation.RestController
@RequestMapping("/pentest")
public class RestController {
    @Autowired
    private PentestService pentestService;

    @Autowired
    private ErrorService errorService;

    @Autowired
    private ErrorLocationService errorLocationService;

    @Autowired
    private ErrorFileService errorFileService;

    @GetMapping
    public List<Pentest> getList() {
        return pentestService.getList();
    }

    @GetMapping("/detail/{id}")
    public PentestBo getDetail(@PathVariable Long id) {
        return pentestService.getById(id);
    }

    @PostMapping("/create")
    public PentestBo store(@RequestBody Pentest pentest) {
        return pentestService.add(pentest);
    }

    @GetMapping("/delete/{id}")
    public boolean delete(@PathVariable Long id) {
        return pentestService.remove(id);
    }

    @PostMapping("/update")
    public void update(@RequestBody Pentest pentest) {
        System.out.println(pentest.toString());
        pentestService.update(pentest);
    }

    @GetMapping("/detail/name={name}")
    public List<Pentest> getDetailByName(@PathVariable String name) {
        return pentestService.findByName(name);
    }


    @GetMapping("/export_list")
    public List<Pentest> exportJson() {
        ObjectMapper objectMapper = new ObjectMapper();
        List<Pentest> pentestList = pentestService.getList();
        for (Pentest pentest : pentestList) {
            List<ErrorBo> errorBos = errorService.getListByPentestId(pentest.getId());
            List<Error> errors = new ArrayList<>();
            for (ErrorBo errorBo : errorBos) {
                Error error = Mapping.errorBoToError(errorBo);
                // Lay danh sach error location
                List<ErrorLocationBo> errorLocationBoList = errorLocationService.findByErrorId(errorBo.getId());
                List<ErrorLocation> errorLocations = new ArrayList<>();
                for (ErrorLocationBo errorLocationBo : errorLocationBoList) {
                    errorLocations.add(Mapping.ErrorLocationBoToErrorLocation(errorLocationBo));
                }
                error.setError_location(errorLocations);
                // Lay danh sach error file
                List<ErrorFileBo> errorFileBos = errorFileService.findByErrorId(errorBo.getId());
                List<ErrorFile> errorFiles = new ArrayList<>();
                for (ErrorFileBo errorFileBo : errorFileBos) {
                    errorFiles.add(Mapping.ErrorFileBoToErrorFile(errorFileBo));
                }
                error.setLstFiles(errorFiles);
                errors.add(error);
            }
            pentest.setErrors(errors);
        }
        return pentestList;
    }

    @GetMapping("/export")
    public Pentest exportPentestError(@RequestParam Long id) {
        PentestBo pentestBo = pentestService.getById(id);
        Pentest pentest = Mapping.pentestBoToPentest(pentestBo);
        List<ErrorBo> errorBos = errorService.getListByPentestId(id);
        List<Error> errors = new ArrayList<>();
        for (ErrorBo errorBo : errorBos) {
            Error error = Mapping.errorBoToError(errorBo);

            // Lay danh sach error location
            List<ErrorLocationBo> errorLocationBoList = errorLocationService.findByErrorId(errorBo.getId());
            List<ErrorLocation> errorLocations = new ArrayList<>();
            for (ErrorLocationBo errorLocationBo : errorLocationBoList) {
                errorLocations.add(Mapping.ErrorLocationBoToErrorLocation(errorLocationBo));
            }
            error.setError_location(errorLocations);

            // Lay danh sach error file
            List<ErrorFileBo> errorFileBos = errorFileService.findByErrorId(errorBo.getId());
            List<ErrorFile> errorFiles = new ArrayList<>();
            for (ErrorFileBo errorFileBo : errorFileBos) {
                errorFiles.add(Mapping.ErrorFileBoToErrorFile(errorFileBo));
            }
            error.setLstFiles(errorFiles);
            errors.add(error);
        }
        pentest.setErrors(errors);
        return pentest;
    }

}
