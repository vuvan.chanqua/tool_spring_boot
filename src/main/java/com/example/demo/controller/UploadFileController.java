package com.example.demo.controller;

import com.example.demo.dao.ErrorFileDAO;
import com.example.demo.entity.ErrorFileBo;
import com.example.demo.model.ErrorFile;
import com.example.demo.model.PentestCheckListError;
import com.example.demo.model.UploadForm;
import com.example.demo.utils.MediaTypeUtils;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.json.simple.parser.ParseException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.ByteArrayResource;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.ServletContext;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

import java.io.FileReader;
import java.util.Iterator;

import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;

@org.springframework.web.bind.annotation.RestController
@RequestMapping("/errorFile")
public class UploadFileController {

    private static String ROOT_DIR = System.getProperty("user.dir");
    private static String SUB_DIR = "/upload/pentest/error";
    private static String UPLOAD_DIR = ROOT_DIR + SUB_DIR;

    @Autowired
    private ErrorFileDAO errorFileDAO;

    private static String getFileExtension(String fileName) {
        if(fileName.lastIndexOf(".") != -1 && fileName.lastIndexOf(".") != 0)
            return fileName.substring(fileName.lastIndexOf(".")+1);
        else return "";
    }

    @PostMapping("/upload")
    public ResponseEntity<?> uploadFileMulti(@ModelAttribute UploadForm form) throws Exception {

        List<ErrorFile>  result = new ArrayList<>();
        try {

            result = this.saveUploadedFiles(form.getFiles());

        }
        catch (IOException e) {
            e.printStackTrace();
            return new ResponseEntity<>("Error: " + e.getMessage(), HttpStatus.BAD_REQUEST);
        }

        return new ResponseEntity<List<ErrorFile>>(result, HttpStatus.OK);
    }

    // Save Files
    private List<ErrorFile> saveUploadedFiles(MultipartFile[] files) throws IOException {

        // Make sure directory exists!
        File uploadDir = new File(UPLOAD_DIR);
        uploadDir.mkdirs();

//        StringBuilder sb = new StringBuilder();
        List<ErrorFile> sb = new ArrayList<>();


        for (MultipartFile file : files) {
            String uniqueID = UUID.randomUUID().toString();
            if (file.isEmpty()) {
                continue;
            }
            String uploadFilePath = UPLOAD_DIR + "/" + uniqueID + "." + getFileExtension(file.getOriginalFilename());
            String uploadFileName = uniqueID + "." + getFileExtension(file.getOriginalFilename());

            byte[] bytes = file.getBytes();
            Path path = Paths.get(uploadFilePath);
            Files.write(path, bytes);

            ErrorFile errorFile = new ErrorFile();
            errorFile.setFileName(file.getOriginalFilename());
            errorFile.setFileCode(uploadFileName);
            errorFile.setFilePath(SUB_DIR);
//            sb.append(uploadFilePath).append(", ");
            sb.add(errorFile);
        }
        return sb;
    }

    @GetMapping("/getAllFiles")
    public List<String> getListFiles() {
        File uploadDir = new File(UPLOAD_DIR);

        File[] files = uploadDir.listFiles();

        List<String> list = new ArrayList<String>();
        for (File file : files) {
            list.add(file.getName());
        }
        return list;
    }

    // http://localhost:8080/download2?fileName=abc.zip
    // Using ResponseEntity<ByteArrayResource>
    @Autowired
    private ServletContext servletContext;

    @GetMapping("/dowload_file")
    public ResponseEntity<ByteArrayResource> downloadFile2(
            @RequestParam String filepath,
            @RequestParam String filecode,
            @RequestParam String actor
            ) throws IOException {

        ErrorFileBo errorFileBo = errorFileDAO.findByFileCode(filecode);

        MediaType mediaType = MediaTypeUtils.getMediaTypeForFileName(this.servletContext, filepath);

        Path path = Paths.get(ROOT_DIR + filepath + "/" + filecode);
        byte[] data = Files.readAllBytes(path);
        ByteArrayResource resource = new ByteArrayResource(data);

        return ResponseEntity.ok()
                .header(HttpHeaders.CONTENT_DISPOSITION, "attachment;filename=" + errorFileBo.getFileName())
                .contentType(mediaType)
                .contentLength(data.length)
                .body(resource);
    }

}
