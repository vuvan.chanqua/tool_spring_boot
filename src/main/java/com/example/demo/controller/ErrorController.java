package com.example.demo.controller;


import com.example.demo.entity.ErrorBo;
import com.example.demo.entity.ErrorFileBo;
import com.example.demo.entity.ErrorLocationBo;
import com.example.demo.mapping.Mapping;
import com.example.demo.model.Error;
import com.example.demo.model.ErrorFile;
import com.example.demo.model.ErrorLocation;
import com.example.demo.model.PentestCheckListError;
import com.example.demo.service.error.ErrorService;
import com.example.demo.service.error_file.ErrorFileService;
import com.example.demo.service.error_location.ErrorLocationService;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

@org.springframework.web.bind.annotation.RestController
@RequestMapping("/error")
public class ErrorController {

    private static final Logger LOGGER = LoggerFactory.getLogger(MainController.class);

    private static String ROOT_DIR = System.getProperty("user.dir");

    @Autowired
    private ErrorService errorService;

    @Autowired
    private ErrorLocationService errorLocationService;

    @Autowired
    private ErrorFileService errorFileService;

    @GetMapping("/detail/pentest_id={id}")
    public ErrorBo findByPentest(@PathVariable Long id) {
        return errorService.findByPentestId(id);
    }

    @GetMapping("/list/pentest_id={id}")
    public List<ErrorBo> allByPentest(@PathVariable Long id){
        return errorService.getListByPentestId(id);
    }

    @GetMapping("/detail/{id}")
    public Error findById(@PathVariable Long id) {
        Error error = Mapping.errorBoToError(errorService.getById(id));
        List<ErrorLocationBo> errorLocationBos = errorLocationService.findByErrorId(id);
        List<ErrorFileBo> errorFileBos = errorFileService.findByErrorId(id);

        List<ErrorLocation> errorLocations =  new ArrayList<>();
        List<ErrorFile> errorFiles =  new ArrayList<>();

        for (ErrorLocationBo errorLocationBo: errorLocationBos){
            ErrorLocation errorLocation = Mapping.ErrorLocationBoToErrorLocation(errorLocationBo);
            errorLocations.add(errorLocation);
        }
        for (ErrorFileBo errorFileBo: errorFileBos){
            errorFiles.add(Mapping.ErrorFileBoToErrorFile(errorFileBo));
        }

        error.setError_location(errorLocations);
        error.setLstFiles(errorFiles);

        return error;
    }

    @PostMapping("/update")
    public Error update(@RequestBody Error error){
        errorService.update(error);
        return null;
    }

    @PostMapping("/create")
    public Error create(@RequestBody Error error){
        errorService.add(error);
        return null;
    }

    @GetMapping("/delete/{id}")
    public boolean update(@PathVariable Long id){
        return errorService.remove(id);
    }


    @GetMapping("/getCheckListError")
    public List<PentestCheckListError> readJSonDB() throws IOException, ParseException {
        LOGGER.info("ROOT_DIR: " + ROOT_DIR);
        JSONParser parser = new JSONParser();

        //JSON parser object to parse read file
        JSONParser jsonParser = new JSONParser();
        List<PentestCheckListError> pentestCheckListErrors = new ArrayList<>();

        try (FileReader reader = new FileReader(ROOT_DIR+"/data/pentest_checklist_error.json"))
        {
            //Read JSON file
            Object obj = jsonParser.parse(reader);

            JSONArray jsonArrayList = (JSONArray) obj;
            System.out.println(jsonArrayList);

            //Iterate over employee array
            jsonArrayList.forEach(
                    emp -> pentestCheckListErrors.add(parseEmployeeObject( (JSONObject) emp ) )
            );


        } catch (IOException | ParseException e) {
            e.printStackTrace();
        }
        return pentestCheckListErrors;
    }

    private PentestCheckListError parseEmployeeObject(JSONObject jsonObject)
    {
        //Get employee object within list
        JSONObject employeeObject = (JSONObject) jsonObject.get("_id");
        String _id = (String) employeeObject.get("$oid");
        String vvi = (String) jsonObject.get("vvi");
        String vci = (String) jsonObject.get("vci");
        String category = (String) jsonObject.get("category");
        String entry = (String) jsonObject.get("entry");
        String sub_entry = (String) jsonObject.get("sub_entry");
        String priority = (String) jsonObject.get("priority");
        String note = (String) jsonObject.get("note");

        PentestCheckListError pentestCheckListError =
                new PentestCheckListError(_id, vvi, vci, category, entry, sub_entry, priority, note);
        System.out.println(pentestCheckListError.toString());
        return  pentestCheckListError;
    }
}
