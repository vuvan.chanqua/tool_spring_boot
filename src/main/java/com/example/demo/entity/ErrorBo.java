package com.example.demo.entity;

import javax.persistence.*;
import java.util.Date;

@Entity
@Table(name = "ERROR")
public class ErrorBo {

    @Id
    @GeneratedValue
    @Column(name = "id")
    private Long id;

    @Column(name = "code")
    private String code;

    @Column(name = "name")
    private String name;

    @Column(name = "vulnerability")
    private String vulnerability;

    @Column(name = "module_system_id")
    private String module_system_id;

    @Column(name = "error_description")
    private String error_description;

    @Column(name = "risk")
    private String risk;

    @Column(name = "step")
    private String step;

    @Column(name = "reason")
    private String reason;

    @Column(name = "solution")
    private String solution;

    @Column(name = "pentest_id")
    private Long pentestId;

    @Column(name = "cat_vulnerability_id")
    private String cat_vulnerability_id;

    @Column(name = "cat_vulnerability_name")
    private String cat_vulnerability_name;

    @Column(name = "create_date")
    private String create_date;

    @Column(name = "create_user_id")
    private String create_user_id;

    @Column(name = "is_search")
    private int is_search;

    @Column(name = "level")
    private String level;

    @Column(name = "status")
    private int status;

    @Column(name = "vci")
    private String vci;

    @Column(name = "vvi")
    private String vvi;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getVulnerability() {
        return vulnerability;
    }

    public void setVulnerability(String vulnerability) {
        this.vulnerability = vulnerability;
    }

    public String getModule_system_id() {
        return module_system_id;
    }

    public void setModule_system_id(String module_system_id) {
        this.module_system_id = module_system_id;
    }

    public String getError_description() {
        return error_description;
    }

    public void setError_description(String error_description) {
        this.error_description = error_description;
    }

    public String getRisk() {
        return risk;
    }

    public void setRisk(String risk) {
        this.risk = risk;
    }

    public String getStep() {
        return step;
    }

    public void setStep(String step) {
        this.step = step;
    }

    public String getReason() {
        return reason;
    }

    public void setReason(String reason) {
        this.reason = reason;
    }

    public String getSolution() {
        return solution;
    }

    public void setSolution(String solution) {
        this.solution = solution;
    }

    public Long getPentest_id() {
        return pentestId;
    }

    public void setPentest_id(Long pentest_id) {
        this.pentestId = pentest_id;
    }

    public String getCat_vulnerability_id() {
        return cat_vulnerability_id;
    }

    public void setCat_vulnerability_id(String cat_vulnerability_id) {
        this.cat_vulnerability_id = cat_vulnerability_id;
    }

    public String getCat_vulnerability_name() {
        return cat_vulnerability_name;
    }

    public void setCat_vulnerability_name(String cat_vulnerability_name) {
        this.cat_vulnerability_name = cat_vulnerability_name;
    }

    public String getCreate_date() {
        return create_date;
    }

    public void setCreate_date(String create_date) {
        this.create_date = create_date;
    }

    public String getCreate_user_id() {
        return create_user_id;
    }

    public void setCreate_user_id(String create_user_id) {
        this.create_user_id = create_user_id;
    }

    public int getIs_search() {
        return is_search;
    }

    public void setIs_search(int is_search) {
        this.is_search = is_search;
    }

    public String getLevel() {
        return level;
    }

    public void setLevel(String level) {
        this.level = level;
    }

    public int getStatus() {
        return status;
    }

    public void setStatus(int status) {
        this.status = status;
    }

    public String getVci() {
        return vci;
    }

    public void setVci(String vci) {
        this.vci = vci;
    }

    public String getVvi() {
        return vvi;
    }

    public void setVvi(String vvi) {
        this.vvi = vvi;
    }
}

