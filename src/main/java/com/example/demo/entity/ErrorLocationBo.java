package com.example.demo.entity;

import javax.persistence.*;

@Entity
@Table(name = "ERROR_LOCATION")
public class ErrorLocationBo {

    @Id
    @GeneratedValue
    @Column(name = "id", nullable = false)
    private Long id;

    @Column(name = "url", length = 255)
    private String url;

    @Column(name = "location", length = 255)
    private String location;

    @Column(name = "note", length = 255)
    private String note;

    @Column(name = "error_id", length = 255, nullable = false)
    private Long errorId;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public String getLocation() {
        return location;
    }

    public void setLocation(String location) {
        this.location = location;
    }

    public String getNote() {
        return note;
    }

    public void setNote(String note) {
        this.note = note;
    }

    public Long getError_id() {
        return errorId;
    }

    public void setError_id(Long error_id) {
        this.errorId = error_id;
    }
}
