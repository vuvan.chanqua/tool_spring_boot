package com.example.demo.entity;

import javax.persistence.*;

@Entity
@Table(name = "ERROR_FILE")
public class ErrorFileBo {

    @Id
    @GeneratedValue
    @Column(name = "id")
    private Long id;

    @Column(name = "filePath")
    private String filePath;

    @Column(name = "fileName")
    private String fileName;

    @Column(name = "fileCode")
    private String fileCode;

    @Column(name = "downloadUrl")
    private String downloadUrl;

    @Column(name = "description")
    private String description;

    @Column(name = "error_id", length = 255)
    private Long errorId;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getFilePath() {
        return filePath;
    }

    public void setFilePath(String filePath) {
        this.filePath = filePath;
    }

    public String getFileName() {
        return fileName;
    }

    public void setFileName(String fileName) {
        this.fileName = fileName;
    }

    public String getFileCode() {
        return fileCode;
    }

    public void setFileCode(String fileCode) {
        this.fileCode = fileCode;
    }

    public String getDownloadUrl() {
        return downloadUrl;
    }

    public void setDownloadUrl(String downloadUrl) {
        this.downloadUrl = downloadUrl;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Long getError_id() {
        return errorId;
    }

    public void setError_id(Long error_id) {
        this.errorId = error_id;
    }
}
