package com.example.demo.service.error_file;

import com.example.demo.dao.ErrorFileDAO;
import com.example.demo.entity.ErrorFileBo;
import com.example.demo.model.ErrorFile;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class ErrorFileServiceIml implements ErrorFileService {

    @Autowired
    private ErrorFileDAO errorFileDAO;

    @Override
    public List<ErrorFileBo> findByErrorId(Long errorId) {
        return errorFileDAO.findAllByErrorId(errorId);
    }

    @Override
    public boolean remove(Long id) {
        errorFileDAO.deleteById(id);
        return true;
    }
}
