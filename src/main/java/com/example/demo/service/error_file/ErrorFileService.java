package com.example.demo.service.error_file;

import com.example.demo.entity.ErrorFileBo;
import com.example.demo.entity.ErrorLocationBo;

import java.util.List;

public interface ErrorFileService {

    List<ErrorFileBo> findByErrorId(Long errorId);

    boolean remove(Long id);
}
