package com.example.demo.service.error_location;

import com.example.demo.dao.ErrorLocationDAO;
import com.example.demo.entity.ErrorLocationBo;
import com.example.demo.model.ErrorLocation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class ErrorLocationServiceIml implements ErrorLocationService {

    @Autowired
    private ErrorLocationDAO errorLocationDAO;

    @Override
    public List<ErrorLocationBo> findByErrorId(Long errorID) {
        return errorLocationDAO.findAllByErrorId(errorID);
    }

    @Override
    public boolean remove(Long id) {
        errorLocationDAO.deleteById(id);
        return true;
    }

    @Override
    public void deleteAllByErrorId(Long id) {
        errorLocationDAO.deleteAllByErrorId(id);
    }

}
