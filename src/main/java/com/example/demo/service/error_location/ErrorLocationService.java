package com.example.demo.service.error_location;

import com.example.demo.entity.ErrorLocationBo;
import com.example.demo.model.ErrorLocation;
import org.springframework.stereotype.Service;

import java.util.List;

public interface ErrorLocationService {
    List<ErrorLocationBo> findByErrorId(Long errorID);

    boolean remove(Long id);

    void deleteAllByErrorId(Long id);


}
