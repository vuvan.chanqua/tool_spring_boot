package com.example.demo.service.error;

import com.example.demo.dao.ErrorDAO;
import com.example.demo.dao.ErrorFileDAO;
import com.example.demo.dao.ErrorLocationDAO;
import com.example.demo.entity.ErrorBo;
import com.example.demo.entity.ErrorFileBo;
import com.example.demo.entity.ErrorLocationBo;
import com.example.demo.mapping.Mapping;
import com.example.demo.model.Error;
import com.example.demo.model.ErrorFile;
import com.example.demo.model.ErrorLocation;
import com.example.demo.utils.DateTimeUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Date;
import java.util.List;

@Service
public class ErrorServiceIml implements ErrorService {

    @Autowired
    private ErrorDAO errorDAO;

    @Autowired
    private ErrorLocationDAO errorLocationDAO;

    @Autowired
    private ErrorFileDAO errorFileDAO;

    @Override
    public List<ErrorBo> getList() {
        return errorDAO.findAll();
    }

    @Override
    public List<ErrorBo> getListByPentestId(Long pentest_id) {
        return errorDAO.findAllByPentestId(pentest_id);
    }

    @Override
    public ErrorBo getById(Long id) {
        return errorDAO.findById(id).get();
    }

    @Override
    public ErrorBo findByPentestId(Long id) {
        return errorDAO.findByPentestId(id);
    }

    @Override
    public boolean remove(Long id) {
        errorDAO.deleteById(id);

        errorLocationDAO.deleteAllByErrorId(id);

        errorFileDAO.deleteAllByErrorId(id);

        return true;
    }

    @Override
    public ErrorBo add(Error error) {
        ErrorBo errorBo = Mapping.ErrorToErrorBo(error);
        errorBo.setPentest_id(error.getPentest_id());
        errorBo.setId(error.getId());
        List<ErrorLocation> errorLocations = error.getError_location();
        List<ErrorFile> errorFiles = error.getLstFiles();

        Date now = new Date();
        String currentISODate = DateTimeUtils.getISO8601StringForDate(now);
        errorBo.setCreate_date(currentISODate);
        System.out.println(currentISODate);

        ErrorBo errorBo1 = errorDAO.save(errorBo);

        addLocation(errorLocations, errorBo1);
        addFile(errorFiles, errorBo1);



        return errorBo1;
    }

    @Override
    public ErrorBo update(Error error) {
        ErrorBo errorBo = errorDAO.findById(error.getId()).get();

        List<ErrorLocation> errorLocations = error.getError_location();
        List<ErrorFile> errorFiles = error.getLstFiles();

        errorBo.setCode(error.getCode());
        errorBo.setName(error.getName());
        errorBo.setVulnerability(error.getVulnerability());
        errorBo.setRisk(error.getRisk());
        errorBo.setStep(error.getStep());
        errorBo.setReason(error.getReason());
        errorBo.setSolution(error.getSolution());
        errorBo.setCreate_date(error.getCreate_date());
        errorBo.setCat_vulnerability_id(error.getCat_vulnerability_id());
        errorBo.setCat_vulnerability_name(error.getCat_vulnerability_name());
        errorBo.setCreate_user_id(error.getCreate_user_id());
        errorBo.setError_description(error.getError_description());
        errorBo.setLevel(error.getLevel());
        errorBo.setIs_search(error.getIs_search());
        errorBo.setModule_system_id(error.getModule_system_id());
        errorBo.setPentest_id(error.getPentest_id());
        errorBo.setStatus(error.getStatus());
        errorBo.setVci(error.getVci());
        errorBo.setVvi(error.getVvi());

        ErrorBo errorBo1 = errorDAO.save(errorBo);


        addLocation(errorLocations, errorBo1);
        addFile(errorFiles, errorBo1);

        return errorBo1;
    }

    private void addLocation(List<ErrorLocation> errorLocations, ErrorBo errorBo) {
        if (errorLocations != null) {
            errorLocationDAO.deleteAllByErrorId(errorBo.getId());
            for (ErrorLocation errorLocation : errorLocations) {
                ErrorLocationBo errorLocationBo = Mapping.ErrorLocationToErrorLocationBo(errorLocation);
                errorLocationBo.setError_id(errorBo.getId());
                errorLocationDAO.save(errorLocationBo);
            }
        }
    }

    private void addFile(List<ErrorFile> errorFiles, ErrorBo errorBo) {
        if (errorFiles != null) {
            errorFileDAO.deleteAllByErrorId(errorBo.getId());
            for (ErrorFile errorFile : errorFiles) {
                ErrorFileBo errorFileBo = Mapping.ErrorFileToErrorFileBo(errorFile);
                errorFileBo.setError_id(errorBo.getId());
                errorFileDAO.save(errorFileBo);
            }
        }
    }
}
