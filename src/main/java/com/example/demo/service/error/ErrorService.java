package com.example.demo.service.error;

import com.example.demo.entity.ErrorBo;
import com.example.demo.entity.ErrorFileBo;
import com.example.demo.entity.ErrorLocationBo;
import com.example.demo.model.Error;
import com.example.demo.model.Pentest;

import java.util.List;

public interface ErrorService {
    List<ErrorBo> getList();

    List<ErrorBo> getListByPentestId(Long pentest_id);

    ErrorBo getById(Long id);

    ErrorBo findByPentestId(Long id);

    boolean remove(Long id);

    ErrorBo add(Error error);

    ErrorBo update(Error error);
}
