package com.example.demo.dao;

import com.example.demo.entity.ErrorLocationBo;
import org.springframework.data.repository.CrudRepository;

import javax.transaction.Transactional;
import java.util.List;

public interface ErrorLocationDAO extends CrudRepository<ErrorLocationBo, Long> {
    List<ErrorLocationBo> findAllByErrorId(Long id);

    @Transactional
    void deleteAllByErrorId(Long id);
}
