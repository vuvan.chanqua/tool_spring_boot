package com.example.demo.dao;

import com.example.demo.entity.ErrorFileBo;
import com.example.demo.model.ErrorFile;
import org.springframework.data.jpa.repository.JpaRepository;

import javax.transaction.Transactional;
import java.util.List;

public interface ErrorFileDAO extends JpaRepository<ErrorFileBo, Long> {
    List<ErrorFileBo> findAllByErrorId(Long id);
    ErrorFileBo findByFilePath(String path);
    ErrorFileBo findByFileCode(String code);

    @Transactional
    void deleteAllByErrorId(Long id);
}
