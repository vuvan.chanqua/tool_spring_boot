package com.example.demo.dao;

import com.example.demo.entity.ErrorBo;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface ErrorDAO extends JpaRepository<ErrorBo, Long> {
    ErrorBo findByPentestId(Long pentestId);
    List<ErrorBo> findAllByPentestId(Long pentestId);
}
