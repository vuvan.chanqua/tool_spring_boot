package com.example.demo.mapping;

import com.example.demo.entity.ErrorBo;
import com.example.demo.entity.ErrorFileBo;
import com.example.demo.entity.ErrorLocationBo;
import com.example.demo.entity.PentestBo;
import com.example.demo.model.Error;
import com.example.demo.model.ErrorFile;
import com.example.demo.model.ErrorLocation;
import com.example.demo.model.Pentest;

public class Mapping {

    public static Error errorBoToError(ErrorBo errorBo) {
        Error error = new Error();
        if (errorBo.getId()!= null){
            error.setId(errorBo.getId());
        }
        error.setCode(errorBo.getCode());
        error.setName(errorBo.getName());
        error.setVulnerability(errorBo.getVulnerability());
        error.setRisk(errorBo.getRisk());
        error.setStep(errorBo.getStep());
        error.setReason(errorBo.getReason());
        error.setSolution(errorBo.getSolution());
        error.setCreate_date(errorBo.getCreate_date());
        error.setCat_vulnerability_id(errorBo.getCat_vulnerability_id());
        error.setCat_vulnerability_name(errorBo.getCat_vulnerability_name());
        error.setCreate_user_id(errorBo.getCreate_user_id());
        error.setError_description(errorBo.getError_description());
        error.setLevel(errorBo.getLevel());
        error.setIs_search(errorBo.getIs_search());
        error.setModule_system_id(errorBo.getModule_system_id());
        error.setPentest_id(errorBo.getPentest_id());
        error.setStatus(errorBo.getStatus());
        error.setVci(errorBo.getVci());
        error.setVvi(errorBo.getVvi());
        return error;
    }

    public static ErrorLocation ErrorLocationBoToErrorLocation(ErrorLocationBo errorLocationBo) {
        ErrorLocation errorLocation = new ErrorLocation();
        errorLocation.setNote(errorLocationBo.getNote());
        errorLocation.setError_id(errorLocationBo.getError_id());
        errorLocation.setLocation(errorLocationBo.getLocation());
        errorLocation.setUrl(errorLocationBo.getUrl());
        return errorLocation;
    }

    public static ErrorFile ErrorFileBoToErrorFile(ErrorFileBo errorFileBo) {
        ErrorFile errorFile = new ErrorFile();
        errorFile.setDescription(errorFileBo.getDescription());
        if (errorFileBo.getId()!=null){
            errorFile.setId(errorFileBo.getId());
        }
        errorFile.setErrorId(errorFileBo.getError_id());
        errorFile.setFileName(errorFileBo.getFileName());
        errorFile.setFilePath(errorFileBo.getFilePath());
        errorFile.setFileCode(errorFileBo.getFileCode());
        errorFile.setDownloadUrl(errorFileBo.getDownloadUrl());
        return errorFile;
    }

    public static ErrorFileBo ErrorFileToErrorFileBo(ErrorFile errorFile) {
        ErrorFileBo errorFileBo = new ErrorFileBo();
        if (errorFile.getId()!=null){
            errorFileBo.setId(errorFile.getId());
        }
        errorFileBo.setDescription(errorFile.getDescription());
        errorFileBo.setError_id(errorFile.getErrorId());
        errorFileBo.setFileName(errorFile.getFileName());
        errorFileBo.setFileCode(errorFile.getFileCode());
        errorFileBo.setFilePath(errorFile.getFilePath());
        errorFileBo.setDownloadUrl(errorFile.getDownloadUrl());
        return errorFileBo;
    }

    public static ErrorBo ErrorToErrorBo(Error error) {
        ErrorBo errorBo = new ErrorBo();
        if (error.getId()!= null){
            errorBo.setId(error.getId());
        }
        errorBo.setCode(error.getCode());
        errorBo.setName(error.getName());
        errorBo.setVulnerability(error.getVulnerability());
        errorBo.setRisk(error.getRisk());
        errorBo.setStep(error.getStep());
        errorBo.setReason(error.getReason());
        errorBo.setSolution(error.getSolution());
        errorBo.setCreate_date(error.getCreate_date());
        errorBo.setCat_vulnerability_id(error.getCat_vulnerability_id());
        errorBo.setCat_vulnerability_name(error.getCat_vulnerability_name());
        errorBo.setCreate_user_id(error.getCreate_user_id());
        errorBo.setError_description(error.getError_description());
        errorBo.setLevel(error.getLevel());
        errorBo.setIs_search(error.getIs_search());
        errorBo.setModule_system_id(error.getModule_system_id());
        errorBo.setPentest_id(error.getPentest_id());
        errorBo.setStatus(error.getStatus());
        errorBo.setVci(error.getVci());
        errorBo.setVvi(error.getVvi());
        return errorBo;
    }

    public static PentestBo pentestToPentestBo(Pentest pentest) {
        PentestBo pentestBo = new PentestBo();
        if (pentest.getId()!=null){
            pentestBo.setId(pentest.getId());
        }
        pentestBo.setName(pentest.getName());
        pentestBo.setCreate_date(pentest.getCreate_date());
        return pentestBo;
    }

    public static Pentest pentestBoToPentest(PentestBo pentestBo) {
        Pentest pentest = new Pentest();
        if (pentestBo.getId()!=null){
            pentest.setId(pentestBo.getId());
        }
        pentest.setName(pentestBo.getName());
        pentest.setCreate_date(pentestBo.getCreate_date());
        return pentest;
    }


    public static ErrorLocationBo ErrorLocationToErrorLocationBo(ErrorLocation errorLocation) {
        ErrorLocationBo errorLocationBo = new ErrorLocationBo();
        errorLocationBo.setNote(errorLocation.getNote());
        errorLocationBo.setError_id(errorLocation.getError_id());
        errorLocationBo.setLocation(errorLocation.getLocation());
        errorLocationBo.setUrl(errorLocation.getUrl());
        return errorLocationBo;
    }
}
