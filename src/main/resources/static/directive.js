app.directive('fileModel', ['$parse', function ($parse) {
    return {
        restrict: 'A',
        link: function (scope, element, attrs) {
            var model = $parse(attrs.fileModel);
            var modelSetter = model.assign;

            element.bind('change', function () {
                scope.$apply(function () {
                    modelSetter(scope, element[0].files[0]);
                });
            });
        }
    };

}]);

app.directive('ngEnter', function () {
    return function (scope, element, attrs) {
        element.bind("keydown keypress", function (event) {
            if (event.which === 13) {
                scope.$apply(function () {
                    scope.$eval(attrs.ngEnter);
                });

                event.preventDefault();
            }
        });
    };
});


app.directive('multiLevelInput', function () {
    return {
        template: '<div class="wrap-multi-level ">' +
            '<b class="dd-multi-level"></b>' +
            '<div style="position: absolute; top: 0; left: 0; width: 100%; height: 100%" ng-click="multiView.onClickParent()"></div>' +
            '<input name="vulnerability" class="input-multi-level-disabled form-control" placeholder="{{placeholder}}" ng-model="multiView.textShow"> <!--<div style=" width: 100%; position: relative; background: white; z-index: 9999; height: 55px; border: 1px solid #ccc; border: 1px solid rgba(0,0,0,0.15); border-radius: 4px; -webkit-box-shadow: 0 6px 12px rgba(0,0,0,0.175); box-shadow: 0 6px 12px rgba(0,0,0,0.175); background-clip: padding-box; " ng-show="multiView.isShowContent"><input class="input-multi-level" style=""></div>-->' +
            '<div class="wrap-input-multi-level" ng-show="multiView.isShowContent"> ' +
            '<span class="no-data-ml" ng-show="firstArray.length == 0"></span>' +
            '<div class="level-1-ml" ng-repeat="root in firstArray" ng-init="secondArray=rootData[$index]"> ' +
            '<span class="span-ml bold"> {{ root }}</span><span style="float: right;">({{ (multiView.uniqueObjectEntry(secondArray)).length || 0 }})</span> '
            + '<div class="wrap-lv2" ng-init="mainRoot2=multiView.uniqueObjectEntry(secondArray)"> '
            + '<div class="level-2-ml" ng-repeat="root2 in mainRoot2" ng-style="multiView.isCanTapToEntry(secondArray, root2) == true ? { cursor : \'pointer\' } : { cursor : \'context-menu\' }"  ng-init="mainRoot3=multiView.filteredSubEntry(secondArray, root2)"  ng-click="multiView.onClickEntry($event, multiView.getTapEntry(secondArray, root2))"> <span class="span-ml-2 span-ml">{{ root2["entry"] }}</span><span style="float: right;">({{ mainRoot3.length }})</span> '
            + '<div class="wrap-lv3" ng-show="mainRoot3.length > 0"> '
            + '<div class="level-3-ml" ng-repeat="root3 in mainRoot3" ng-click="multiView.onClickSubEntry($event, root3)"> <span class="span-ml-3 span-ml">{{ root3.sub_entry }}</span> </div> </div> </div> </div> </div> </div> </div>'
            + '<style>.bold {font-weight: bold;} .wrap-multi-level{ position: relative; } .dd-multi-level{ position: absolute; right: 0; top: 0; bottom: 0; width: 35px; cursor: pointer; } .input-multi-level-disabled{ cursor: pointer !important; background-color: #ffffff; border: 1px solid #e6ecf5; border-radius: 2px; width: 100%;  } .wrap-input-multi-level { background-color: #fff; border: 1px solid #ccc; border: 1px solid rgba(0,0,0,0.15); border-radius: 4px; -webkit-box-shadow: 0 6px 12px rgba(0,0,0,0.175); box-shadow: 0 6px 12px rgba(0,0,0,0.175); background-clip: padding-box; position: absolute; top: 36px; left: 0; right: 0; background: white; z-index: 9999999999; border-radius: 4px; border: 1px solid #bbbbbb; cursor: pointer; } .level-1-ml { position: relative; height: 100%; cursor: context-menu !important; padding: 9px;} .level-1-ml:hover .wrap-lv2{ display: unset; } .wrap-lv2{ background: white; width: 60%; position: absolute; left: 100%; top: 0; padding-left: 3px; display: none; border: 1px solid #ccc; border: 1px solid rgba(0,0,0,0.15); border-radius: 4px;} .wrap-lv2::before { width: 36px; content: ""; position: absolute; left: -36px; height: 100%; } .level-2-ml { position: relative; width: 100%; cursor: pointer; border-radius: 4px; padding: 9px; margin-top: 3px; } .level-2-ml:hover .wrap-lv3{ display: unset; } .wrap-lv3{ background: white; width: 100%; position: absolute; left: 100%; top: 0; padding-left: 3px; display: none;border: 1px solid #ccc; border: 1px solid rgba(0,0,0,0.15); border-radius: 4px; } .wrap-lv3::before { width: 36px; content: ""; position: absolute; left: -36px; height: 100%; } .level-3-ml { position: relative; width: 100%; cursor: pointer; border-radius: 4px; padding: 9px; margin-top: -1px; } .input-multi-level { padding: 9px; width: calc(100% - 17.5px); border-radius: 3px; border: 1px solid gainsboro; user-select: none; outline: none; margin: 8.5px; } .no-data-ml { width: 100%; display: inline-block; text-align: center; font-size: 12px; padding: 8px; } .span-ml{ word-break: break-word; } </style>',
        restrict: 'E',
        scope: {
            rootData: '=',
            firstArray: '=',
            placeholder: '=',
            model: '=',
            onChange: '='
        },
        link: function (scope, element, attrs) {
            scope.multiView = {};
            scope.multiView.textShow = "";
            scope.multiView.isShowContent = false;
            scope.multiView.onClickParent = function () {
                scope.multiView.isShowContent = !scope.multiView.isShowContent;
            };
            scope.multiView.isCanTapToEntry = function (firstList, entryName) {
                var subEntry = [];
                firstList.forEach(item => {
                    if (item.entry == entryName.entry) {
                        subEntry.push(item);
                    }
                });
                if (subEntry.length == 1 && subEntry[0].sub_entry.length == 0) {
                    return true;
                } else if (subEntry.length > 1) {
                    for (var i = 0; i < subEntry.length; i++) {
                        let item = subEntry[i];
                        if (item.sub_entry.length == 0) {
                            return true;
                        }
                    }
                } else {
                    return false;
                }
            };

            scope.multiView.getTapEntry = function (firstList, entryName) {
                var subEntry = [];
                firstList.forEach(item => {
                    if (item.entry == entryName.entry) {
                        subEntry.push(item);
                    }
                });
                if (subEntry.length == 1 && subEntry[0].sub_entry.length == 0) {
                    return subEntry[0];
                } else if (subEntry.length > 1) {
                    for (var i = 0; i < subEntry.length; i++) {
                        let item = subEntry[i];
                        if (item.sub_entry.length == 0) {
                            return item;
                        }
                    }
                } else {
                    return null;
                }
            };

            scope.multiView.onClickEntry = function (event, entry) {
                if (entry == null || !(event.target.className.includes("level-2-ml") || event.target.className.includes("span-ml-2"))) {
                    return;
                }
                scope.multiView.textShow = entry.entry;
                scope.model = entry;
                scope.multiView.isShowContent = false;
                scope.onChange(entry);
            };
            scope.multiView.onClickSubEntry = function (event, sub_entry) {
                if (sub_entry == null || !(event.target.className.includes("level-3-ml") || event.target.className.includes("span-ml-3"))) {
                    return;
                }
                scope.multiView.textShow = sub_entry.sub_entry;
                scope.model = sub_entry;
                scope.multiView.isShowContent = false;
                scope.onChange(sub_entry);
            };


            scope.multiView.filteredSubEntry = function (firstList, entryName) {
                var subEntry = [];
                firstList.forEach(item => {
                    if (item.entry == entryName.entry && item.sub_entry != null && item.sub_entry != "") {
                        subEntry.push(item);
                    }
                });
                return subEntry;
            };

            scope.multiView.splitLine = function (collection) {
                var temp = collection.split("\n");
                var newCollection = [];
                temp.forEach(item => {
                    if (item) {
                        newCollection.push(item);
                    }
                });
                return newCollection;
            };
            scope.multiView.uniqueObjectEntry = function (collection) {
                var keyname = "entry";
                var output = [],
                    keys = [];
                angular.forEach(collection, function (item) {
                    var key = item[keyname];
                    if (keys.indexOf(key) === -1) {
                        keys.push(key);
                        output.push(item);
                    }
                });
                return output;
            };
            $(document).bind('click', function (event) {
                var isClickedElementChildOfPopup = element
                    .find(event.target)
                    .length > 0;
                if (isClickedElementChildOfPopup)
                    return;
                scope.$apply(function () {
                    scope.multiView.isShowContent = false;
                });
            });
        },
        controller: function ($scope) {
            $scope.$watch('model', function (ne, ol) {
                if (typeof (ne) == 'string') {
                    $scope.multiView.textShow = ne;
                } else {
                    if (ne.sub_entry != "") {
                        $scope.multiView.textShow = ne.entry + ' - ' + ne.sub_entry;
                    }
                }
            });
        }
    };
});