var app = angular.module("App", ['ngFileUpload']);

// Controller Part
app.controller("AppController", function ($scope, $http, Upload, $timeout) {

    $scope.error_location = [];
    $scope.lstFiles = [];
    $scope.pentests = [];
    $scope.errorLists = [];
    $scope.pentestDetail = {};

    $scope.errorLocation = {};

    $scope.pentestChecklistError = [];
    $scope.vulnerabilities = {};
    $scope.vulnerabilities.rootData = [];
    $scope.vulnerabilities.categories = [];
    $scope.vulnerabilities.entry = {};


    $scope.isCreated = 0;

    $scope.errorDeleteId = {};
    $scope.pentestDeleteId = {};
    $scope.lstFilesNames = [];

    // Error list start
    $scope.confirmDeleteErrorById = (id) => {
        $scope.errorDeleteId = id;
        $("#confirm-delete-error").modal("show");
    };

    $scope.deleteErrorById = () => {
        $http.get("/error/delete/" + $scope.errorDeleteId).then((res) => {
            $scope.getListError($scope.pentestDetail.id);
            $("#confirm-delete-error").modal("hide");
            console.log(res);
        });
    };

    $scope.openPopupDetailError = (id) => {
        $scope.isCreated = 2;
        $http.get("/error/detail/" + id).then((res) => {
            $scope.error = res.data;
            console.log(res);
            $scope.error_location = res.data.error_location;
            $scope.lstFiles = res.data.lstFiles;

            $scope.vulnerabilities.entry = res.data.cat_vulnerability_name;
            $scope.lstFiles.forEach(function (item) {
                item.name = item.fileName;
            });
        });
        $scope.getCheckListError();
        $("#add-error-modal").modal("show");
    };
    // Error list end


    // index start
    $scope.openPopupListError = (pentest) => {
        $scope.getListError(pentest.id);
        console.log(pentest);
        $scope.pentestName = pentest.name;
        $scope.pentestDetail = pentest;
        $("#list-error-modal").modal('show');
    };

    $scope.getListError = (pentest_id) => {
        $http.get("/error/list/pentest_id=" + pentest_id).then((res) => {
            console.log(res);
            $scope.errorLists = res.data;
        });
    };

    $scope.findErrorByName = () => {
        $http.get("/pentest/detail/name=" + $scope.search).then((res) => {
            $scope.pentests = res.data;
        });
    };

    $scope.getErrorDataByPentest = (pentest_id) => {
        $http.get("/error/detail/pentest_id=" + pentest_id).then((res) => {
            console.log(res);
            $scope.error = res.data;
        }, (err) => {
            console.log(err)
        })
    };

    $scope.deletePentestById = () => {
        $http.get("/pentest/delete/" + $scope.pentestDeleteId).then((res) => {
            $("#confirm-delete-pentest").modal("hide");
            $scope.getListPentest();
        }, (err) => {
            console.log(err)
        })
    };

    $scope.confirmDeletePentest = (id) => {
        $scope.pentestDeleteId = id;
        console.log(id);
        $("#confirm-delete-pentest").modal("show");
    };

    $scope.addNewErrorToPentest = (pentest) => {
        $scope.error = {};
        $scope.lstFiles = [];
        $scope.error_location = [];
        $scope.pentestChecklistError = [];
        $scope.isCreated = 1;
        console.log($scope.isCreated);
        $scope.pentestDetail = pentest;
        console.log(pentest);
        $scope.pentestName = pentest.name;
        $scope.getCheckListError();
        $("#add-error-modal").modal("show");
    };
    // end index


    $scope.openPopupAddErrorPentest = () => {
        $scope.isCreated = 0;
        console.log($scope.isCreated);
        $scope.pentestDetail = {};
        $scope.pentestName = "";
        $scope.error = {};
        $scope.lstFiles = [];
        $scope.error_location = [];
        $scope.pentestChecklistError = [];
        $scope.getCheckListError();
        $("#add-error-modal").modal('show');
        console.log("Add action");
    };


    $scope.getListPentest = () => {
        $http.get("/pentest").then((res) => {
            console.log(res);
            $scope.pentests = res.data;
        }, (err) => {
            console.log(err)
        })
    };

    // detail error start
    $scope.addErrorLocation = () => {
        $scope.error_location.push(
            {urlFile: "", location: "", note: "", showText: false}
        )
    };

    $scope.removeErrorLocation = (index) => {
        $scope.error_location.splice(index, 1);
    };

    $scope.saveErrorLocation = (index) => {
        let errorLocation = $scope.error_location[index];
        errorLocation.showText = true;
    };

    $scope.getCheckListError = () => {
        $http.get("/error/getCheckListError").then((response) => {
            console.log(response);
            $scope.pentestChecklistError = response.data;
            setTimeout(function () {
                $("#cat_vulnerability_id").change();
                if ($scope.pentestChecklistError.length > 0) {
                    $.each($scope.pentestChecklistError, function (i, el) {
                        if ($.inArray(el['category'], $scope.vulnerabilities.categories) === -1) $scope.vulnerabilities.categories.push(el['category']);
                    });
                    for (var i = 0; i < $scope.vulnerabilities.categories.length; i++) {
                        var temp = [];
                        for (var j = 0; j < $scope.pentestChecklistError.length; j++) {
                            if ($scope.pentestChecklistError[j]['category'] === $scope.vulnerabilities.categories[i]) {
                                temp.push($scope.pentestChecklistError[j]);
                            }
                            if (j === $scope.pentestChecklistError.length - 1) {
                                $scope.vulnerabilities.rootData.push(temp);
                                temp = [];
                            }
                        }
                    }
                }
            });
        }, (err) => {
            $scope.pentestChecklistError = [];
        });
    };

    $scope.onChangeVulnerabilities = function (item) {
        if (item && item.priority) {
            $scope.error.level = item.priority.toString();
            $scope.error.cat_vulnerability_id = item._id;
            $scope.error.vci = item.vci;
            $scope.error.vvi = item.vvi;
            if (item.sub_entry) {
                $scope.error.cat_vulnerability_name = item.entry + ' - ' + item.sub_entry;
            } else {
                $scope.error.cat_vulnerability_name = item.entry;
            }
        } else {
            $scope.error.level = '';
            $scope.error.cat_vulnerability_id = '';
            $scope.error.vci = '';
            $scope.error.vvi = '';
            $scope.error.cat_vulnerability_name = '';
        }
    };

    $scope.save = (isValid) => {
        if (isValid){
            if ($scope.isCreated === 1) {
                // them error moi tu pentest

                let error = {
                        code: $scope.error.code,
                        name: $scope.error.name,
                        vulnerability: $scope.error.vulnerability,
                        level: $scope.error.level,
                        module_system_id: $scope.error.module_system_id,
                        error_description: $scope.error.error_description,
                        risk: $scope.error.risk,
                        step: $scope.error.step,
                        pentest_id: $scope.pentestDetail.id,
                        reason: $scope.error.reason,
                        solution: $scope.error.solution,
                        error_location: $scope.error_location,
                        lstFiles: $scope.lstFiles,


                        cat_vulnerability_id: $scope.error.cat_vulnerability_id,
                        vci: $scope.error.vci,
                        vvi: $scope.error.vvi,
                        cat_vulnerability_name: $scope.error.cat_vulnerability_name
                    }
                ;
                $http.post("/error/create", error).then((res) => {
                    console.log(res);
                    $scope.getListPentest();
                    $("#add-error-modal").modal("hide");
                }, (err) => {
                    console.log(err)
                })
            } else if ($scope.isCreated === 0) {
                // them pentest moi
                let submitForm = {
                    name: $scope.pentestName,
                    errors: [
                        {
                            code: $scope.error.code,
                            name: $scope.error.name,
                            vulnerability: $scope.error.vulnerability,
                            level: $scope.error.level,
                            module_system_id: $scope.error.module_system_id,
                            error_description: $scope.error.error_description,
                            risk: $scope.error.risk,
                            step: $scope.error.step,
                            reason: $scope.error.reason,
                            solution: $scope.error.solution,
                            error_location: $scope.error_location,
                            lstFiles: $scope.lstFiles,

                            cat_vulnerability_id: $scope.error.cat_vulnerability_id,
                            vci: $scope.error.vci,
                            vvi: $scope.error.vvi,
                            cat_vulnerability_name: $scope.error.cat_vulnerability_name
                        }
                    ]
                };
                $http.post("/pentest/create", submitForm).then((res) => {
                    console.log(res);
                    $("#add-error-modal").modal("hide");
                    $scope.getListPentest();
                }, (err) => {
                    console.log(err)
                })
            } else {
                // cap nhat error
                let error = {
                    id: $scope.error.id,
                    code: $scope.error.code,
                    name: $scope.error.name,
                    vulnerability: $scope.error.vulnerability,
                    level: $scope.error.level,
                    module_system_id: $scope.error.module_system_id,
                    error_description: $scope.error.error_description,
                    risk: $scope.error.risk,
                    step: $scope.error.step,
                    pentest_id: $scope.pentestDetail.id,
                    reason: $scope.error.reason,
                    solution: $scope.error.solution,
                    error_location: $scope.error_location,
                    lstFiles: $scope.lstFiles,

                    cat_vulnerability_id: $scope.error.cat_vulnerability_id,
                    vci: $scope.error.vci,
                    vvi: $scope.error.vvi,
                    cat_vulnerability_name: $scope.error.cat_vulnerability_name
                };
                $http.post("/error/update", error).then((res) => {
                    console.log(res);
                    $scope.getListPentest();
                    $scope.getListError($scope.pentestDetail.id);
                    $("#add-error-modal").modal("hide");
                }, (err) => {
                    console.log(err)
                })
            }
            $scope.lstFiles = [];
        }
    };
    // detail error end


    // file start
    $scope.exportPentest = (pentest) =>{
        $http.get("/pentest/export?id=" + pentest.id).then((response) => {
            if (typeof response == 'undefined' || response == null || response === "")
                return;

            let link = document.createElement("a");
            link.download = "pentest.json";
            let data = "text/json;charset=utf-8," + encodeURIComponent(JSON.stringify(response.data, null, 2));
            link.href = "data:" + data;
            link.click();
        })
    };

    $scope.exportListPentest = () => {
        $http.get("/pentest/export_list").then((response) => {
            if (typeof response == 'undefined' || response == null || response === "")
                return;

            let link = document.createElement("a");
            link.download = "pentest_list.json";
            let data = "text/json;charset=utf-8," + encodeURIComponent(JSON.stringify(response.data, null, 2));
            link.href = "data:" + data;
            link.click();
        })
        // return encodeURIComponent(JSON.stringify(phone));
    };

    $scope.uploadErrorFile = (files) => {
        $scope.lstFiles.push(...files)
    };
    $scope.removeErrorFile = (index) => {
        $scope.lstFiles.splice(index, 1);
    };

    $scope.uploadFiles = function (files) {
        $scope.files = files;

        // $scope.lstFiles.push(...files);

        if (files && files.length) {
            Upload.upload({
                url: '/errorFile/upload',
                data: {
                    files: files
                }
            }).then(function (response) {
                $timeout(function () {
                    $scope.result = response.data;
                    response.data.forEach(function (item) {
                        item.name = item.fileName;
                        $scope.lstFiles.push(item);
                    });
                    console.log(response.data);
                });
            }, function (response) {
                if (response.status > 0) {
                    $scope.errorMsg = response.status + ': ' + response.data;
                }
            }, function (evt) {
                $scope.progress =
                    Math.min(100, parseInt(100.0 * evt.loaded / evt.total));
            });
        }
    };

    $scope.downloadErrorFile = (errorFile) => {
        window.location.assign("/errorFile/dowload_file/?filepath=" + errorFile.filePath + "&filecode=" + errorFile.fileCode + "&actor=thaovt");
    };
    // file end

    $scope.getListPentest();

});